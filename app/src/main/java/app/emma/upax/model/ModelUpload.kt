package app.emma.upax.model

import android.net.Uri
import app.emma.upax.firebase.FireBaseConnection
import app.emma.upax.model.data.ImageOnline
import app.emma.upax.utils.FileUploadListener
import app.emma.upax.utils.ModelUtils.toImage
import app.emma.upax.utils.Uploader
import app.emma.upax.viewmodel.UploadViewModel

class ModelUpload(private val viewModel: UploadViewModel) {

    fun getImages(){
        FireBaseConnection.initCollectionImages()
            .addSnapshotListener { snapshot, ex ->
                val list : MutableList<ImageOnline> = mutableListOf()
                if (snapshot != null){
                    snapshot.forEach { image ->
                        list.add(image.toImage()!!)
                    }
                    viewModel.images.postValue(list)
                }

                if (ex != null){
                    viewModel.errorImages.postValue(ex.message)
                }
            }
    }

    fun uploadImage(uri: Uri){
        Uploader.uploadImage(uri, "image/jpg", object : FileUploadListener {
            override fun onFileUploaded(url: String) {
                FireBaseConnection.initCollectionImages().add(ImageOnline(url))
                    .addOnSuccessListener {
                        viewModel.upload.postValue(true)
                    }
                    .addOnFailureListener {
                        viewModel.errorUpload.postValue(it.message)
                    }
            }

            override fun onFileUploadError(error: String?) {
                viewModel.errorUpload.postValue(error)
            }
        })
    }
}