package app.emma.upax.model

import app.emma.upax.api.Service
import app.emma.upax.constants.ApiCredentials
import app.emma.upax.viewmodel.MoviesViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ModelMoviesOnline(val viewModel: MoviesViewModel) {

    private val api = Service()

    fun getMovies(){
        api.api.getMovies(ApiCredentials.API_KEY).enqueue(object : Callback<MoviesResponse> {
            override fun onResponse(call: Call<MoviesResponse>, response: Response<MoviesResponse>) {
                if (response.isSuccessful){
                    val movies = response.body()!!.results
                    viewModel.movies.postValue(movies)
                } else{
                    viewModel.error.postValue(response.message())
                }
            }

            override fun onFailure(call: Call<MoviesResponse>, t: Throwable) {
                viewModel.exception.postValue("Error en el servicio")
            }
        })
    }
}