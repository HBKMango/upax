package app.emma.upax.model

import android.content.Context
import androidx.room.Room
import app.emma.upax.database.MovieDB
import app.emma.upax.utils.ModelUtils
import app.emma.upax.viewmodel.MoviesViewModel

class ModelMoviesLocal(private val viewModel: MoviesViewModel) {
    suspend fun getMovies(context: Context){
        val room = Room
            .databaseBuilder(context, MovieDB::class.java, "Movie")
            .build()

        val list = room.movieDAO().getMovies()
        viewModel.movies.postValue(ModelUtils.toMovieList(list))
    }
}