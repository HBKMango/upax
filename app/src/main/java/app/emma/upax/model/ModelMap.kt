package app.emma.upax.model

import app.emma.upax.firebase.FireBaseConnection
import app.emma.upax.model.data.Location
import app.emma.upax.utils.ModelUtils.toLocation
import app.emma.upax.viewmodel.MapViewModel

class ModelMap(private val viewModel: MapViewModel) {

    fun getLocations(){
        FireBaseConnection.initCollectionLocations()
            .addSnapshotListener { snapshot, ex ->
                val list : MutableList<Location> = mutableListOf()

                if (snapshot != null){
                    snapshot.forEach { location ->
                        list.add(location.toLocation()!!)
                    }
                    viewModel.locations.postValue(list)
                }

                if (ex != null){
                    viewModel.error.postValue(ex.message)
                }
            }
    }
}