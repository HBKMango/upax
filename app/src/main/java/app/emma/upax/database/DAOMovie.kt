package app.emma.upax.database

import androidx.room.*
import app.emma.upax.model.MovieDataBase

@Dao
interface DAOMovie {
    @Query("SELECT * FROM MovieDataBase")
    suspend fun getMovies(): List<MovieDataBase>

    @Query("SELECT * FROM MovieDataBase WHERE MovieDataBase.id = :id")
    suspend fun getMovie(id : Int): MovieDataBase

    @Insert
    suspend fun addMovie(movies : List<MovieDataBase>)

    @Update
    suspend fun updateMovie(movie: MovieDataBase)

    @Delete
    suspend fun deleteMovie(movie: MovieDataBase)
}