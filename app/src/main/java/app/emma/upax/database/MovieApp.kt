package app.emma.upax.database

import android.app.Application
import androidx.room.Room

class MovieApp : Application() {
    val room = Room
        .databaseBuilder(this, MovieDB::class.java, "Movie")
        .build()
}