package app.emma.upax.viewmodel

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.emma.upax.model.ModelUpload
import app.emma.upax.model.data.ImageOnline

class UploadViewModel : ViewModel(){

    val images = MutableLiveData<List<ImageOnline>>()
    val upload = MutableLiveData<Boolean>()
    val errorUpload = MutableLiveData<String>()
    val errorImages = MutableLiveData<String>()

    val model = ModelUpload(this)

    fun getImages(){
        model.getImages()
    }

    fun uploadImage(uri: Uri){
        model.uploadImage(uri)
    }
}