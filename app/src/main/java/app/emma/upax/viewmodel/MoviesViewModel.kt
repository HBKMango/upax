package app.emma.upax.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import app.emma.upax.model.ModelMoviesLocal
import app.emma.upax.model.ModelMoviesOnline
import app.emma.upax.model.Movie
import kotlinx.coroutines.launch

class MoviesViewModel(private val context: Context) : ViewModel() {

    val movies = MutableLiveData<List<Movie>>()
    val error = MutableLiveData<String>()
    val exception = MutableLiveData<String>()

    private val modelOnline = ModelMoviesOnline(this)
    private val modelLocal = ModelMoviesLocal(this)

    fun getMovies(){
        viewModelScope.launch {
            modelOnline.getMovies()
        }
    }

    fun getMoviesFromDB(context: Context){
        viewModelScope.launch {
            modelLocal.getMovies(context)
        }
    }
}

class MoviesViewModelFactory(private val context: Context) : ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = MoviesViewModel(context) as T
}