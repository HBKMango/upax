package app.emma.upax.api

import app.emma.upax.constants.ApiCredentials
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Service {
    val api: ApiEndPoints

    init {
        val login = HttpLoggingInterceptor()
        login.level = HttpLoggingInterceptor.Level.BODY

        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.addInterceptor(login)

        val client = clientBuilder.build()
        val retrofit = Retrofit.Builder()
            .baseUrl(ApiCredentials.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        api = retrofit.create(ApiEndPoints::class.java)
    }
}