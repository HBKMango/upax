package app.emma.upax.view.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import app.emma.upax.R
import app.emma.upax.databinding.FragmentMapBinding
import app.emma.upax.viewmodel.MapViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

class MapFragment : Fragment(), GoogleMap.OnMarkerClickListener {

    lateinit var binding: FragmentMapBinding
    private lateinit var load : LoadFragment
    private val viewModel : MapViewModel by viewModels()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        load = LoadFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getLocations()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View {
        binding = FragmentMapBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    private val callback = OnMapReadyCallback { googleMap ->
        googleMap.setOnMarkerClickListener(this@MapFragment)

        try {
            viewModel.locations.observe(viewLifecycleOwner, { locations ->
                val marker = MarkerOptions()

                if (locations.isNotEmpty()){
                    locations!!.forEach{
                        val location = LatLng(it.lat, it.lng)

                        marker
                            .position(location)
                            .title("Ubicación")

                        googleMap.addMarker(marker)
                    }

                    val zoom = locations[locations.size - 1]

                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(zoom.lat, zoom.lng), 13f))
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onMarkerClick(p0: Marker): Boolean {
        return true
    }
}