package app.emma.upax.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.work.Constraints
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import app.emma.upax.databinding.ActivityMainBinding
import app.emma.upax.utils.BackgroundLocations
import app.emma.upax.view.adapters.PageAdapter
import com.google.android.material.tabs.TabLayout
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    lateinit var adapter : PageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        adapter = PageAdapter(supportFragmentManager, this)

        binding.tab.tabGravity = TabLayout.GRAVITY_FILL
        binding.viewPager.adapter = adapter
        binding.tab.setupWithViewPager(binding.viewPager)

        val constraints = Constraints.Builder ()
            .setRequiresCharging (true)
            .build ()

        val saveRequest =
            PeriodicWorkRequest.Builder(BackgroundLocations::class.java, 3, TimeUnit.MINUTES)
                .setConstraints(constraints)
                .build()

        val workManager = WorkManager.getInstance(applicationContext)
        workManager.enqueue(saveRequest)
    }
}