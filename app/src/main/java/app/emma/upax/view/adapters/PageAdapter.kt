package app.emma.upax.view.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import app.emma.upax.R
import app.emma.upax.view.fragments.MapFragment
import app.emma.upax.view.fragments.MoviesFragment
import app.emma.upax.view.fragments.UploadFragment

class PageAdapter(fm: FragmentManager, private val context: Context) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                return MoviesFragment()
            }
            1 -> {
                return MapFragment()
            }
            2 -> {
                return UploadFragment()
            }
        }
        return MoviesFragment()
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence {
        when (position) {
            0 -> {
                return context.getString(R.string.peliculas)
            }
            1 -> {
                return context.getString(R.string.mapa)
            }
            2 -> {
                return context.getString(R.string.galeria)
            }
        }
        return "Principal"
    }
}