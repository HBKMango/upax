package app.emma.upax.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import app.emma.upax.R
import app.emma.upax.databinding.FragmentLoadBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class LoadFragment : DialogFragment() {

    lateinit var binding: FragmentLoadBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
        isCancelable = false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentLoadBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null){
            dialog.window!!.setLayout(
                resources.displayMetrics.widthPixels * 1,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }
    }
}