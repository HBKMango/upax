package app.emma.upax.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import app.emma.upax.R
import app.emma.upax.databinding.FragmentMoviePreviewBinding
import app.emma.upax.model.Movie
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class MoviePreviewFragment(private val movie: Movie) : BottomSheetDialogFragment() {

    lateinit var binding: FragmentMoviePreviewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentMoviePreviewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.toolBar.setNavigationOnClickListener{
            dismiss()
        }

        binding.description.text = "${movie.overview}\n${movie.overview}\n${movie.overview}\n${movie.overview}"
        binding.totalVote.text = movie.vote_count.toString()
        binding.release.text = movie.release_date
        binding.languaje.text = movie.original_language
        binding.titleOriginal.text = movie.original_title

        val imageLand = "https://image.tmdb.org/t/p/w500/"+movie.backdrop_path
        val imagePost = "https://image.tmdb.org/t/p/w500/"+movie.poster_path

        binding.ratingBar.rating = (movie.vote_average/2).toFloat()

        Glide.with(requireContext())
            .load(imageLand)
            .error(ContextCompat.getDrawable(requireContext(), R.drawable.landscape))
            .into(binding.imageLand)

        Glide.with(requireContext())
            .load(imagePost)
            .into(binding.imagePost)

        binding.CollapsinToolBar.title = movie.title
    }
}