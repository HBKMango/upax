package app.emma.upax.view.adapters

import android.annotation.SuppressLint
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.emma.upax.databinding.ItemMovieBinding
import app.emma.upax.model.Movie
import com.bumptech.glide.Glide

class MoviesAdapter : RecyclerView.Adapter<MoviesAdapter.MovieViewHolder>() {

    private var list: List<Movie> = ArrayList()
    lateinit var listener: MovieListener

    @SuppressLint("NotifyDataSetChanged")
    fun swapData(list: List<Movie>){
        this.list = list
        notifyDataSetChanged()
    }

    class MovieViewHolder(private val binding: ItemMovieBinding) : RecyclerView.ViewHolder(binding.root){
        private var listener: MovieListener? = null
        private var lastClickTime: Long = 0

        fun bind(item:Movie, listener: MovieListener, position: Int) = with(binding) {
            this@MovieViewHolder.listener = listener

            val imageUri = "https://image.tmdb.org/t/p/w500/"+item.poster_path

            Glide.with(binding.root.context)
                .load(imageUri)
                .into(image)

            root.setOnClickListener{
                if (SystemClock.elapsedRealtime() - lastClickTime < 2000){
                    return@setOnClickListener
                }

                this@MovieViewHolder.listener!!.onMovieSelected(item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) =
        holder.bind(list[position], listener, position)

    override fun getItemCount(): Int = list.size
}

interface MovieListener{
    fun onMovieSelected(movie: Movie)
}