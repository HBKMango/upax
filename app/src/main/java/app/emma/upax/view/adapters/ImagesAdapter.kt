package app.emma.upax.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.emma.upax.databinding.ItemImageBinding
import app.emma.upax.model.data.ImageOnline
import com.bumptech.glide.Glide

class ImagesAdapter :RecyclerView.Adapter<ImagesAdapter.ImageviewHolder>() {
    private var list: List<ImageOnline> = ArrayList()

    fun swapData(list: List<ImageOnline>) {
        this.list = list
        notifyDataSetChanged()
    }

    class ImageviewHolder(private val binding: ItemImageBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item: ImageOnline) = with(binding){
            Glide.with(binding.root.context)
                .load(item.image)
                .into(image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageviewHolder {
        val view = ItemImageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ImageviewHolder(view)
    }

    override fun onBindViewHolder(holder: ImageviewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int = list.size
}