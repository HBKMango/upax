package app.emma.upax.utils

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import app.emma.upax.firebase.FireBaseConnection
import app.emma.upax.model.data.Location
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener

class BackgroundLocations(private val context: Context, workerParams: WorkerParameters)
    : Worker(context, workerParams){

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun doWork(): Result {
        getLastKnownLocation()
        return Result.success()
    }

    private fun getLastKnownLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

        Dexter.withContext(context)
            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : PermissionListener {
                @SuppressLint("MissingPermission")
                override fun onPermissionGranted(response: PermissionGrantedResponse) {
                    fusedLocationClient.lastLocation
                        .addOnSuccessListener { location->
                            if (location != null) {
                                sendLocation(Location(location.latitude, location.longitude))
                            }
                        }
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse) {
                    Log.e("LOCATION", "Sin permisos de ubicación")
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?) {
                }
            }).check()
    }

    fun sendLocation(location: Location){
        FireBaseConnection.initCollectionLocations().add(location)
            .addOnSuccessListener {
                Log.e("LOCATION", "Ubicación enviada")
            }.addOnFailureListener {
                Log.e("LOCATION", "Error al enviar ubicación")
            }
    }
}