package app.emma.upax.utils

import android.net.Uri
import app.emma.upax.firebase.FireBaseConnection
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.StorageMetadata
import com.google.firebase.storage.UploadTask
import java.io.File

object Uploader {

    fun uploadImage(uri: Uri, type: String, fileUploadListener: FileUploadListener) {
        uploadFile(uri, type, fileUploadListener)
    }

    private fun uploadFile(uri: Uri, type: String, fileUploadListener: FileUploadListener){

        val uploadUri = Uri.fromFile(File(uri.toString()))

        val storageRef = FireBaseConnection.initStorage().reference

        val imageRef = storageRef.child("images/${System.currentTimeMillis()}")
        val metadata = StorageMetadata.Builder()
                .setContentType(type)
                .build()
        val uploadTask = imageRef.putFile(uploadUri, metadata)

        uploadTask.addOnFailureListener {
            fileUploadListener.onFileUploadError(it.message)
        }.addOnSuccessListener {
            uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                return@Continuation imageRef.downloadUrl
            }).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val downloadUri = task.result
                    fileUploadListener.onFileUploaded(downloadUri.toString())
                } else {
                    fileUploadListener.onFileUploadError("Error")
                }
            }
        }
    }
}

interface FileUploadListener {
    fun onFileUploaded(url: String)
    fun onFileUploadError(error: String?)
}